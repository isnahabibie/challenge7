const {round, match, rooms, user_game_history} = require("../models")

exports.fight = async (req,res,next) => {
    const room_id = req.params.room_id                          // localhost:3000/fight/api/v1/1669641824865
    const room = await rooms.findOne({where:{name:room_id}})
    let active_round = await round.get_active_round(room.id)    // Fungsi untuk mendapatkan ronde yang sedang aktif, jadi tidak perlu memasukkan ronde ke berapa di postman nya
    console.log(room.id)
    if (active_round <= 3){
        const count = await match.count({
            where: {
                room_id: room.id,
                round: active_round
            },
        });
    
        if (count < 2) {
            const fight = await match.create({
                user_id: req.user.id,
                room_id: room.id,
                choice: req.body.choice,
                round: active_round
            })
            .then(async fight => {
                const winner_id = await match.findWinner(fight.room_id, fight.round)                // Fungsi untuk mencari pemenang, definitionnya ada di models/match

                if (winner_id !== null || winner_id !== undefined) {
                    var record = await round.record_winner(fight.room_id, fight.round, winner_id)   // Fungsi untuk mencatat siapa yang menang ke tabel round, ada di models/round
                    const match_data = await match.findAll({where:{room_id: fight.room_id}})
                    if(winner_id == 0){
                        return res.status(200).json({
                            message: "We tie! Result is Draw!",
                            data: match_data
                        })
                    }else{
                        return res.status(200).json({
                            message: "User " + winner_id + " wins!",
                            data:match_data
                        })
                    }
                } else{
                    return res.status(200).json({
                        message: "Waiting for opponent"
                    })
                }
            })
            .catch(() => {
                return res.status(200).json({
                    message: "Waiting for opponent"
                })
            })
        } else {
            const winner_id = await match.findWinner(room.id, active_round)                     // Fungsi untuk mencari pemenang, definitionnya ada di models/match
            const record_winner = await round.record_winner(room.id, active_round, winner_id)   // Fungsi untuk mencatat siapa yang menang ke tabel round, ada di models/round
            return res.status(200).json(record_winner)
        }
    } else {
        var winner_of_this_room = await round.get_round_data(room.id)   // Fungsi untuk mencari siapa yang punya skor terbanyak dalam 1 room (3 ronde), ada di models/round
        await user_game_history.score_record(winner_of_this_room)       // Fungsi untuk mencatat score ke tabel user_game_histories, ada di models/user_game_history

        return res.status(200).json({
            message: "Game is over! Room " + room.name + " has 3 round matches"
        })
    }
}