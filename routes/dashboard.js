const express = require('express')
const router = express.Router()
const dashboard = require("../controllers/dashboardController")
const restrict = require("../middlewares/restrict")

router.get("/", restrict, dashboard.index)        // http://localhost:3000/dashboard
router.get("/about", restrict, dashboard.about)   // http://localhost:3000/dashboard/about

module.exports = router